#!/usr/bin/env python2
import os
import math
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import sys

e = os.system

neural_cli = 'bin/neural_cli' if len(sys.argv) <= 1 or sys.argv[1] != '--use_gpu' else 'bin/neural_cli_gpu'

e('mkdir -p gen')
N = 60

X = []
Y = []

ts = open('gen/ts.dat', 'w')
for i in range(0, N):
    x = 1.0/(N+1) * i
    y = 0.5*math.sin(x*6*math.pi)+0.5 #1.0/(math.e**x)
    ts.write("2 1 %f\n1 %f\n\n" % (x, y))
    X.append(x)
    Y.append(y)

ts.close()
plt.scatter(X, Y, color='red')

e('{0} create gen/sin.net 2 100 100 1'.format(neural_cli))
e('{0} train gen/sin.net gen/ts.dat 1 2000'.format(neural_cli))

X = []

f = open('gen/input.dat', 'w')
for i in range(0, N*10):
    x = 1.0/(N*10+1) * i
    X.append(x)
    f.write("2 1 %f\n" % x)
f.close()

e('{0} run gen/sin.net gen/input.dat > gen/output.dat'.format(neural_cli))

f = open('gen/output.dat', 'r')
Y = [float(a) for a in ' '.join([line.strip() for line in f]).split()]
f.close()

plt.scatter(X, Y, color='blue')

plt.savefig("gen/sin.png")

#!/usr/bin/env python2

import os
import sys
import glob

e = os.system

neural_cli = 'bin/neural_cli' if len(sys.argv) <= 1 or sys.argv[1] != '--use_gpu' else 'bin/neural_cli_gpu'

e('mkdir -p gen')

e('./build.py')
if not glob.glob('images/*.jpg'):
    e('cd images; python get_images.py; cd ..')

N = 300
e('bin/image_to_input trainset {0} images/K*.jpg > gen/images_train.dat'.format(N))
n_in, n_out = 1, 1
with open('gen/images_train.dat') as f:
    nbrs = f.readline().split()
    n_in = int(nbrs[0])
    n_out = int(nbrs[n_in + 1])
e('{0} create gen/images.net {1} 100 100 {2}'.format(neural_cli, n_in, n_out))
for i in range(10):
    e('{1} train gen/images.net gen/images_train.dat {0} 3000'.format(1 - i * 0.1, neural_cli))
    print("Done: {0}%".format((i+1)* 10))

e('bin/image_to_input input {0} images/P*.jpg > gen/images_in.dat'.format(N))
e('{0} run gen/images.net gen/images_in.dat > gen/images_out.dat'.format(neural_cli))

#!/usr/bin/env python2
# coding=utf-8

# Do not generate .pyc files:
import sys
sys.dont_write_bytecode = True

import os
from fabricate import *

cuda_install_path = "/usr/local/cuda"
cuda_available = os.path.isdir(cuda_install_path)
input_dir = "code"
output_dir = "bin"
source_names = ["neural_network", "neural_network_test", "neural_cli", "nanojpeg", "image_to_input", "neural_network_gpu", "neural_network_gpu_test"]
bin_names = ["neural_network_test", "neural_network_gpu_test", "neural_cli", "image_to_input", "neural_cli_gpu"]
sources = [os.path.join(input_dir, name + ".cpp") for name in source_names + ["neural_cli"]]
objects = [os.path.join(output_dir, name + ".o") for name in source_names + ["neural_cli_gpu"]]
cpp_flags = ["-std=gnu++0x", "-Wall", "-Wshadow", "-Wextra"]
flags = [cpp_flags for name in source_names] + [cpp_flags + ["-DUSE_GPU"]]
flags[3] = ["-fpermissive"]
uses_cuda = [False, False, False, False, False, True, True, True]
lib_objects = [os.path.join(output_dir, name + ".o")
        for name in source_names if name not in bin_names]
tests = [os.path.join(".", output_dir, test) for test in ["neural_network_test", "neural_network_gpu_test"]]
test_cuda = [False, True]

def mkdirs():
    run("mkdir", "--parents", output_dir)

def build():
    mkdirs()
    generate_ptx()
    compile()
    link()

def generate_ptx():
    if not cuda_available: return
    run(os.path.join(cuda_install_path, "bin", "nvcc"), "-arch", "sm_20", "-ptx",
        os.path.join(input_dir, "neural_network_gpu.cu"), "-o", os.path.join(output_dir, "neural_network_gpu.ptx"))
    run("mkdir", "--parents", "gen")
    run("cp", os.path.join(output_dir, "neural_network_gpu.ptx"), "gen")

def compile():
    for source, output, flag, use_cuda in zip(sources, objects, flags, uses_cuda):
        if not cuda_available and use_cuda: continue
        gcc_flags = (["-I" + os.path.join(cuda_install_path, "include")] if use_cuda else []) + flag
        run("g++", "-c", source, "-o", output, *gcc_flags)

def link():
    run("g++", "-o", os.path.join(output_dir, "neural_cli"),
         list(os.path.join(output_dir, name + ".o") for name in ("neural_cli", "neural_network")), *cpp_flags)
    run("g++", "-o", os.path.join(output_dir, "neural_network_test"),
        list(os.path.join(output_dir, name + ".o") for name in ("neural_network", "neural_network_test")), *cpp_flags)
    run("g++", "-o", os.path.join(output_dir, "image_to_input"),
            list(os.path.join(output_dir, name + ".o") for name in ("image_to_input", "nanojpeg")), *cpp_flags)

    if cuda_available:
        run("g++", "-o", os.path.join(output_dir, "neural_cli_gpu"),
            list(os.path.join(output_dir, name + ".o") for name in ("neural_cli_gpu", "neural_network_gpu")),
            "-L/usr/lib/nvidia-current", "-lcuda", *cpp_flags)
        run("g++", "-o", os.path.join(output_dir, "neural_network_gpu_test"),
            list(os.path.join(output_dir, name + ".o") for name in ("neural_network_gpu", "neural_network_gpu_test")),
            "-L/usr/lib/nvidia-current", "-lcuda", *cpp_flags)

def test():
    build()
    for test, use_cuda in zip(tests, test_cuda):
        if not cuda_available and use_cuda: continue
        shell(test, silent=False)

def fasttest():
    build()
    for test, use_cuda in zip(tests, test_cuda):
        if not cuda_available and use_cuda: continue
        shell(test, "--fast", silent=False)

def memcheck():
    build()
    for test, use_cuda in zip(tests, test_cuda):
        if not cuda_available and use_cuda: continue
        shell("valgrind", test, "--fast", silent=False)

def clean():
    autoclean()

main()

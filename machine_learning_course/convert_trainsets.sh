#!/bin/bash

mkdir --parent trainset_converted
find trainset -name '*.list' | while read file ; do
  cat "$file" | sed 's#/afs/cs/project/theo-8/faceimages/#./#' > "trainset_converted/$(basename $file)"
done

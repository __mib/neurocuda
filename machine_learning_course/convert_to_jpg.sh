#!/bin/bash

find faces -name '*.pgm' | while read pgm_file ; do
  jpg_file="$(echo "$pgm_file" | sed 's/^faces/faces_converted/' | sed 's/pgm$/jpg/')"
  #echo "Making dir $(dirname "$jpg_file")"
  mkdir --parents "$(dirname "$jpg_file")"
  #echo "Converting $pgm_file to $jpg_file"
  convert "$pgm_file" "$jpg_file"
done

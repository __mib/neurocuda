#!/bin/bash

wget --continue 'http://www.cs.cmu.edu/afs/cs.cmu.edu/project/theo-8/faceimages/faces.tar.Z'
tar xf faces.tar.Z
wget --continue 'http://www.cs.cmu.edu/afs/cs.cmu.edu/project/theo-8/faceimages/docs/hw97.ps'
wget --continue --recursive --no-parent --reject 'index.html*' --no-host-directories --cut-dirs 5 'http://www.cs.cmu.edu/afs/cs.cmu.edu/project/theo-8/faceimages/code/'
wget --continue --recursive --no-parent --reject 'index.html*' --no-host-directories --cut-dirs 5 'http://www.cs.cmu.edu/afs/cs.cmu.edu/project/theo-8/faceimages/trainset/'

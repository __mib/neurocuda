#include "neural_network_gpu.h"
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#define isNan(val) (val != val)

template<typename T>
bool check(T expected, T actual, std::string error_string) {
  if (expected != actual) {
    std::cerr << "ERROR: '" << expected << "' expected, but '" << actual
      << "' found' (" << error_string << ")" << std::endl;
    return false;
  }
  return true;
}

bool compare_files(std::string filename1, std::string filename2) {
  std::ifstream file1, file2;
  file1.open(filename1);
  file2.open(filename2);
  std::string line1, line2;
  while (not (file1.eof() and file2.eof())) {
    if (file1.eof() xor file2.eof()) {
      std::cerr << "ERROR: One file ended earlier: " << file1.eof()
        << " vs. " << file2.eof() << std::endl;
      return false;
    }
    std::getline(file1, line1);
    std::getline(file2, line2);
    if (line1 != line2) {
      std::cerr << "ERROR: Files differ: '" << line1 << "' vs. '"
        << line2 << "'" << std::endl;
      return false;
    }
  }
  return true;
}

bool approx_check(std::vector<double> expected,
    std::vector<double> actual,
    std::string error_string,
    double allowed_error=0.000001) {
  if (not check(expected.size(), actual.size(),
        "wrong size (" + error_string + ")")) {
    return false;
  }
  for (int i = 0; i < static_cast<int>(expected.size()); ++i) {
    if (isNan(actual[i])) {
      std::cerr << "ERROR: " << i << "-th element is NaN" << std::endl;
      return false;
    }
    if (std::fabs(actual[i] - expected[i]) > allowed_error) {
      std::cerr << "ERROR: " << i << "-th element differs: "
        << actual[i] << " vs. " << expected[i] 
        << " (" << error_string << ")" << std::endl;
      return false;
    }
  }
  return true;
}

bool check_learn_many(
    const std::vector<int>& layer_sizes,
    const std::vector<std::vector<double>>& inputs,
    const std::vector<std::vector<double>>& outputs, bool fast) {
  bool ok = true;
  if (fast) return ok;
  NeuralNetwork network(layer_sizes);
  network.LearnMany(1, 10000, inputs, outputs);

  for (int i = 0; i < static_cast<int>(inputs.size()); ++i) {
    ok &= approx_check(outputs[i], network.GetOutput(inputs[i]),
        "wrong output from trained neural network (LearnMany)",
        0.1);
  }
  return ok;
}

int main(int argc, char** argv) {
  srand(17);
  bool fast = (argc > 1 and std::string(argv[1]) == "--fast");
  bool ok = true;
  // Create neural network with 3 layers: 2 input neurons, 4 hidden neurons
  // and 3 output neurons.
  NeuralNetwork network({2, 4, 3});
  ok &= check(3, network.NumberOfLayers(),
      "wrong number of layers in random network");

  bool success = true;
  NeuralNetwork network_from_file = NeuralNetwork::LoadFromFile(
      &success, "testdata/small_network.net");
  ok &= check(true, success, "loading from file failed");
  ok &= check(3, network.NumberOfLayers(),
      "wrong number of layers in network read from file");

  ok &= check(true, network_from_file.SaveToFile("gen/small_network.net"),
      "failed to write neural network to file");
  ok &= check(
      true,
      compare_files("testdata/small_network.net", "gen/small_network.net"),
      "saved file different from original");

  ok &= approx_check(
      {0.615057},
      network_from_file.GetOutput({0.3, 0.7}),
      "wrong output from neural network");


  double* weights1 = new double[1];
  weights1[0] = 0.2;
  double* weights2 = new double[1];
  weights2[0] = 0.5;
  std::vector<double*> weights({weights1, weights2});
  NeuralNetwork small_network({1,1,1}, weights);

  ok &= approx_check(
      {0.56463498},
      small_network.GetOutput({0.4}),
      "wrong output form small neural network");

  if (not fast) {
      NeuralNetwork kait_net_file = NeuralNetwork::LoadFromFile(
          &success, "testdata/kait_network.net");

      for (int i = 0; i < 10000; ++i) {
        kait_net_file.LearnOne(0.1, {0.4, 0.6}, {0.3});
      }

      ok &= approx_check(
          {0.3},
          kait_net_file.GetOutput({0.4, 0.6}),
          "wrong output from trained neural network");
  }

  if (not fast) {
      NeuralNetwork omg_net_file = NeuralNetwork::LoadFromFile(
          &success, "testdata/omg_network.net");

      std::vector <std::vector<double>> input;
      std::vector <std::vector<double>> output;
      int N = 10;
      int iter = 10000;
      for(int i = 0; i < N; i++) input.push_back({1, 1.0/(N+1) * i});
      for(int i = 0; i < N; i++) output.push_back({1 - 1.0/(N+1) * i});

      for (int i = 0; i < iter; ++i) {
          for(int j = 0; j < N; j++) {
            omg_net_file.LearnOne(0.1, input[j], output[j]);
          }
      }

      for(int i = 0; i < N; i++) {
          ok &= approx_check(
              output[i],
              omg_net_file.GetOutput(input[i]),
              "wrong output from trained neural network",
              0.1);
      }
  }

  ok &= approx_check(
      {0.05227876},
      small_network._GetErrors(
        CudaVector(vector<double>({0.4})),
        CudaVector(vector<double>({0.5}))).vector(),
      "wrong error");

  ok &= approx_check(
      vector<double>({-0.0072078}),
      small_network._PropagateErrors(
        CudaVector(vector<double>({-0.06})),
        CudaVector(weights2, 1),
        CudaVector(vector<double>({0.4}))).vector(),
      "wrong error propagation");

  for (int i = 0; i < 300; ++i) {
      network.LearnOne(1, {0, 1}, {0.1, 0.9, 0.1});
  }
  ok &= approx_check(
          {0.1, 0.9, 0.1},
          network.GetOutput({0, 1}),
          "wrong output from trained neural network", 0.01);

  ok &= check_learn_many(
      {2, 4, 1},
      {{1, 0.1}, {1, 0.9}, {1, 0.5}},
      {{0.1}, {0.9}, {0.5}},
      fast);

  ok &= check_learn_many(
      {2, 30, 3},
      {{1, 0}, {0, 1}, {1, 1}},
      {{0.5, 0.1, 0.9}, {0.5, 0.9, 0.1}, {0.9, 0.5, 0.5}},
      fast);

  ok &= check_learn_many(
      {2, 30, 2},
      {{1, 0}, {0, 1}, {0.5, 0.5}},
      {{1, 0}, {0, 1}, {0.5, 0.5}},
      fast);

  if (ok) {
    std::cout << "Everything OK" << std::endl;
  } else {
    std::cout << "THERE WERE ERRORS IN SOME TESTS!" << std::endl;
  }
  return 0;
}

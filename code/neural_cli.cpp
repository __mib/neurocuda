#ifdef USE_GPU
#include "neural_network_gpu.h"
#else
#include "neural_network.h"
#endif
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <cassert>
using namespace std;

int main(int argc, char *argv[]) {
    if (argc < 3) {
        cerr << "Moar args plz. Sth liek: command filename <args...>" << endl;
        return 13;
    }

    cerr << "File: " << argv[2] << endl;

    if (string(argv[1]) == "create") {
        vector<int> layers;
        for(int i=3; i<argc; ++i) layers.push_back( atoi(argv[i]) );
        cerr << "Using " << layers.size() << " layers: ";
        for(int s: layers) cerr << s << ' '; cerr << endl;

        NeuralNetwork net(layers);
        bool status = net.SaveToFile(argv[2]);
        cerr << (net.SaveToFile(argv[2]) ? "Great success!" : "Critical Fail") << endl;
        return status ? 0 : 17;
    }

    cerr << "Loading network" << endl;
    bool success = true;
    NeuralNetwork net(
            NeuralNetwork::LoadFromFile(
                &success, argv[2]));
    if (! success) {
        cerr << "LOL Fail" << endl;
        return 82;
    }

    cerr << "Loaded: NumberOfLayers: " << net.NumberOfLayers() << endl;

    if (string(argv[1]) == "run") {
        cerr << "Use legs to make run." << endl;
        cerr << "Input: " << argv[3] << endl;

        ifstream input(argv[3]);
        int n;
        int caseno = 0;

        while(input >> n) {
            assert(n == net.getInputLayerSize());

            vector<double> data;
            for(int i=0; i<n; ++i) {
                double a; input >> a;
                data.push_back(a);
            }

            cerr << "Running case #" << caseno << endl;
            ++caseno;

            vector<double> output(
                    net.GetOutput(data));

            for(double a: output) cout << a << ' '; cout << endl;
        }
        return 0;
    } else if (string(argv[1]) == "train") {
        vector<vector<double>> inputs, outputs;
        if (argc < 5) {
            cerr << "Expected args: trainset_filename eta iterations" << endl;
            return 12;
        }

        cerr << "I like trains." << endl;
        cerr << "Trainset: " << argv[3] << endl;
        double eta = atof(argv[4]);
        int iterations = atoi(argv[5]);
        cerr << "Eta: " << eta << endl;
        cerr << "Iterations: " << iterations << endl;

        
        ifstream trainset(argv[3]);
        int n;
        int caseno = 0;

        while(trainset >> n) {
            cerr << "Loading case #" << ++caseno << endl;
            vector<double> input, output;
            
            assert(n == net.getInputLayerSize());

            while(n--) {
                double a; trainset >> a;
                input.push_back(a);
            }

            trainset >> n;
            assert(n == net.getOutputLayerSize());

            while(n--) {
                double a; trainset >> a;
                output.push_back(a);
            }

            inputs.push_back(input);
            outputs.push_back(output);
        }

        cerr << "Trainset loaded (" << inputs.size() << " elements)" << endl;

        net.LearnMany(eta, iterations, inputs, outputs);
        cerr << "Zapisuje do " << argv[2] << endl;
        net.SaveToFile(argv[2]);
        return 0;
    } else {
        cerr << "Wut was dat? Expected one of: create, run, train" << endl;
        return 98;
    }

  return 0;
}

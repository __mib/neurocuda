#include "nanojpeg.h"
#include <cstdio>
#include <iostream>
#include <cmath>
using namespace std;

int main(int argc, char* argv[]) {
  if (argc < 4) {
    cerr << "Moar args plz> Sth liek: command input_neurons filename.jpg [more_filenames...]" << endl;
    cerr << "Commands are: input trainset" << endl;
    return 13;
  }
  bool trainset = true;
  if (string(argv[1]) == "trainset") {
      trainset = true;
  } else if (string(argv[1]) == "input") {
      trainset = false;
  } else {
      cerr << "Unknown command: " << argv[1] << endl;
      return 17;
  }
  int n = atoi(argv[2]);
  njInit();

  for (int i = 3; i < argc; ++i) {
      FILE* f = fopen(argv[i], "rb");
      if (!f) {
          cerr << "Could not open: " << argv[i] << ", skipping." << endl;
          continue;
      }
      fseek(f, 0, SEEK_END);
      int size = (int) ftell(f);
      void* buf = malloc(size);
      fseek(f, 0, SEEK_SET);
      size = (int) fread(buf, 1, size, f);
      fclose(f);

       if (njDecode(buf, size)) {
          cerr << "Could not decode: " << argv[i] << ", skipping." << endl;
          free(buf);
          continue;
       }
       if (njIsColor()) {
          cerr << "Color picture: " << argv[i] << ", skipping." << endl;
          free(buf);
          continue;
       }

       int m = njGetImageSize();
       unsigned char* data = njGetImage();

       cout << n;
       int pixels_per_value = (m + n - 1) / n;
       int square_size = sqrt(pixels_per_value) + 1;
       int squares_per_row = (njGetWidth() + square_size - 1) / square_size;
       for (int j = 0; j < n; ++j) {
           int squarex = j % squares_per_row;
           int squarey = j / squares_per_row;
           cout << " ";
           double value = 0;
           int count = 0;
           for (int x = squarex * square_size; x < min((squarex + 1) * square_size, njGetWidth()); ++x) {
             for (int y = squarey * square_size; y < min((squarey + 1) * square_size, njGetHeight()); ++y) {
               count++;
               value += data[njGetWidth() * y + x];
             }
           }
           if (count > 0) value /= count;
           value /= 256;
           cout << value;
       }
       if (trainset) { 
           cout << " " << (argc - 3);
           for (int j = 0; j < argc - 3; ++j) {
               cout << " ";
               if (j == i) cout << "1.0";
               else cout << "0.0";
           }
       }
       cout << endl;
       free(buf);
  }
  njDone();
  return 0;
}

#include<cstdio>
#include<cmath>
extern "C" {
    __global__ void Hello() {
        printf("Hello!\n");
    }
    __global__ void PropagateOneLayer(double* output, int output_size, double* input, int input_size, double* weights) {
        int i = blockDim.x * blockIdx.x + threadIdx.x;
        if (i >= output_size) return;
        int idx = i * input_size;
        double value = 0;
        for (int j = 0; j < input_size; ++j) {
            value += weights[idx++] * input[j];
        }
        output[i] = value;
    }

    __device__ double ActivationFunction(double x) {
        return (1.0 / (1.0 + std::exp(-x)));
    }

    __device__ double ActivationFunctionDerivative(double x) {
        double ex = std::exp(x);
        return ex / ((ex + 1) * (ex + 1));
    }

    __global__ void GetErrors(double* input, double* correct, double* errors, int size) {
        int i = blockDim.x * blockIdx.x + threadIdx.x;
        if (i >= size) return;
        errors[i] = - (correct[i] - ActivationFunction(input[i])) * ActivationFunctionDerivative(input[i]);
    }

    __global__ void ApplyActivationFunction(double* input, double* output, int size) {
        int i = blockDim.x * blockIdx.x + threadIdx.x;
        if (i >= size) return;
        output[i] = ActivationFunction(input[i]);
    }

    __global__ void UpdateWeights(double eta, double* errors, int error_size,
            double* output, int input_size, double* weights) {
        int i = blockDim.x * blockIdx.x + threadIdx.x;
        if (i >= error_size) return;
        int idx = i * input_size;
        for (int j = 0; j < input_size; ++j) {
            double aa = eta * output[j] * errors[i];
            weights[idx] -= aa;
            idx++;
        }
    }

    __global__ void PropagateErrors(
            double* input, int input_size, double* input_errors,
            double* errors, int errors_size, double* weights) {
        int j = blockDim.x * blockIdx.x + threadIdx.x;
        if (j >= input_size) return;
        int idx = j;
        input_errors[j] = 0;
        for (int i = 0; i < errors_size; ++i) {
            input_errors[j] += weights[idx] * errors[i];
            idx += input_size;
        }

        input_errors[j] *= ActivationFunctionDerivative(input[j]);
    }
}

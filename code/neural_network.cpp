#include "neural_network.h"
#include <cstdlib>
#include <fstream>
#include <iostream>

using namespace std;

template <typename T>
std::ostream& operator<<(std::ostream& out, const std::vector<T>& in) {
    out << "[";
    for (const T& t : in) {
        out << t << ", ";
    }
    return out << "]";
}

NeuralNetwork::NeuralNetwork(std::vector<int> layer_sizes) :
    _layer_sizes(layer_sizes), _weights() {
        for (int i = 1; i < static_cast<int>(layer_sizes.size()); ++i) {
            int matrix_size = layer_sizes[i-1] * layer_sizes[i];
            double* weight_matrix = new double[matrix_size];
            for (int j = 0; j < matrix_size; ++j) {
                weight_matrix[j] = -1.0 + static_cast<double>(rand()) / (RAND_MAX / 2);
            }
            _weights.push_back(weight_matrix);
        }
    }

NeuralNetwork::~NeuralNetwork() {
    for (double* weight_matrix : _weights) {
        delete[] weight_matrix;
    }
}

int NeuralNetwork::getInputLayerSize() const {
    return _layer_sizes.size() > 0 ? _layer_sizes[0] : 0;
}

int NeuralNetwork::getOutputLayerSize() const {
    return _layer_sizes.size() > 0 ? _layer_sizes[_layer_sizes.size()-1] : 0;
}

int _ReadPositiveInt(bool* error, std::ifstream* file) {
    int result;
    (*file) >> result;
    if (result <= 0 or file->fail()) {
        *error = true;
    }
    return result;
}

double _ReadWeight(bool* error, std::ifstream* file) {
    double weight;
    (*file) >> weight;
    if (file->fail()) {
      *error = true;
    }
    return weight;
}

NeuralNetwork NeuralNetwork::LoadFromFile(bool* success, std::string filename) {
    std::vector<int> layer_sizes;
    std::vector<double*> weights;
    int layers;
    bool error = false;
    std::ifstream file;
    file.open(filename);
    if (not file.is_open()) {
        error = true;
    } else {
        layers = _ReadPositiveInt(&error, &file);
        if (not error) {
            for (int i = 0; i < layers; ++i) {
                int layer_size = _ReadPositiveInt(&error, &file);
                layer_sizes.push_back(layer_size);
                if (error) {
                    break;
                }
            }
        }
        if (not error) {
            for (int i = 1; i < static_cast<int>(layer_sizes.size()); ++i) {
                int weights_matrix_size = layer_sizes[i - 1] * layer_sizes[i];
                double* weights_matrix = new double[weights_matrix_size];
                weights.push_back(weights_matrix);
                for (int j = 0; j < weights_matrix_size; ++j) {
                    weights_matrix[j] = _ReadWeight(&error, &file);
                    if (error) {
                        break;
                    }
                }
                if (error) break;
            }
        }
    }
    if (error) {
        if (success != NULL) {
            *success = false;
        }
        for (double* weight : weights) {
            delete[] weight;
        }
        return NeuralNetwork({1});
    }
    return NeuralNetwork(layer_sizes, weights);
}

bool NeuralNetwork::SaveToFile(std::string filename) {
    std::ofstream file;
    file.open(filename);
    file << NumberOfLayers() << std::endl;
    bool first = true;
    for (int layer_size : _layer_sizes) {
        if (not first) file << " ";
        first = false;
        file << layer_size;
    }
    file << std::endl;
    for (int i = 1; i < static_cast<int>(_layer_sizes.size()); ++i) {
        file << std::endl;
        int idx = 0;
        for (int j = 0; j < _layer_sizes[i]; ++j) {
            first = true;
            for (int k = 0; k < _layer_sizes[i-1]; ++k) {
                if (not first) file << " ";
                first = false;
                file << _weights[i-1][idx++];
            }
            file << std::endl;
        }
    }
    return file.good();
}

std::vector<double> NeuralNetwork::PropagateOneLayer(
        const std::vector<double>& input,
        const double* weights,
        int output_size) const {
    std::vector<double> output(output_size);
    int idx = 0;
    for (int i = 0; i < output_size; ++i) {
        double value = 0;
        for (int j = 0; j < static_cast<int>(input.size()); ++j) {
            value += weights[idx++] * input[j];
        }
        output[i] = value;
    }
    return output;
}

std::vector<double> NeuralNetwork::ApplyActivationFunction(const std::vector<double>& input) const {
    std::vector<double> output;
    for (double v : input) {
        output.push_back(ActivationFunction(v));
    }
    return output;
}

std::vector<double> NeuralNetwork::GetOutput(
        const std::vector<double>& input) const {
    std::vector<double> output = input;
    for (int i = 1; i < NumberOfLayers(); ++i) {
        output = ApplyActivationFunction(
                PropagateOneLayer(output, _weights[i-1], _layer_sizes[i]));
    }
    return output;
}

std::vector<double> NeuralNetwork::_GetErrors(
        const std::vector<double>& correct,
        const std::vector<double>& input) const {
    std::vector<double> errors(correct.size());
    std::vector<double> actual = ApplyActivationFunction(input);
    for (int i = 0; i < static_cast<int>(correct.size()); ++i) {
        errors[i] = - (correct[i] - actual[i]) * ActivationFunctionDerivative(input[i]);
    }
    return errors;
}

std::vector<double> NeuralNetwork::_PropagateErrors(
        const std::vector<double>& errors,
        const double* weights,
        const std::vector<double>& input) const {
    std::vector<double> input_errors(input.size());
    int idx = 0;
    for (int i = 0; i < static_cast<int>(errors.size()); ++i) {
        for (int j = 0; j < static_cast<int>(input.size()); ++j) {
            input_errors[j] += weights[idx] * errors[i];
            idx++;
        }
    }
    idx = 0;
    for (double& input_error : input_errors) {
        input_error *= ActivationFunctionDerivative(input[idx]);
        idx++;
    }
    return input_errors;
}

void NeuralNetwork::_UpdateWeights(
        double eta,
        const std::vector<double>& errors,
        const std::vector<double>& outputs,
        double* weights,
        int input_size) {
    int idx = 0;
    double max_dist = 0;
    for (int i = 0; i < static_cast<int>(errors.size()); ++i) {
        for (int j = 0; j < input_size; ++j) {
            double aa = eta * outputs[j] * errors[i];
            weights[idx] -= aa;
            max_dist = max(max_dist, fabs(outputs[j] * errors[i]));
            idx++;
        }
    }
}
double NeuralNetwork::_MaxAbsoluteValue(
        const std::vector<double>& value) {
    double res = 0;
    for (double v : value) {
        if (std::fabs(v) > res) res = std::fabs(v);
    }
    return res;
}

void NeuralNetwork::LearnOne(double eta,
        const std::vector<double>& input,
        const std::vector<double>& correct_output) {
    std::vector<std::vector<double>> outputs;
    outputs.push_back(input);
    for (int i = 1; i < NumberOfLayers(); ++i) {
        if (i == 1) {
            //cout << "INPUT:" << endl;
            //cout << outputs.back() << endl;
            std::vector<double> new_output = PropagateOneLayer(
                        outputs.back(),
                        _weights[i-1],
                        _layer_sizes[i]);
            outputs.push_back(new_output);
            ////cout << "OUTPUT:" << endl;
            //cout << outputs.back() << endl;
            //cout << "actual:" << endl;
            //cout << ApplyActivationFunction(outputs.back()) << endl;
        } else {
            std::vector<double> new_output = PropagateOneLayer(
                        ApplyActivationFunction(outputs.back()),
                        _weights[i-1],
                        _layer_sizes[i]);
            outputs.push_back(new_output);
        }
    }
    std::vector<std::vector<double>> errors(NumberOfLayers());
    errors.back() = _GetErrors(correct_output, outputs.back());
    //cout << "first error: " << errors.back() << endl;
    for (int i = NumberOfLayers() - 1; i > 0; --i) {
        errors[i-1] = _PropagateErrors(errors[i], _weights[i-1], outputs[i-1]);
        //cout << "next error: " << errors[i-1] << endl;
        if (i > 1) {
            _UpdateWeights(eta, errors[i], ApplyActivationFunction(outputs[i-1]),
                    _weights[i-1], _layer_sizes[i-1]);
        } else {
            _UpdateWeights(eta, errors[i], outputs[i-1],
                    _weights[i-1], _layer_sizes[i-1]);
        }
    }

    /*
    cout << "weights:" << endl;
    for(int i = 0; i < _weights.size(); i++) {
        for(int j = 0; j < _layer_sizes[i]; j++) {
            cout << _weights[i][j] << " ";
        }
        cout << endl;
    }
    */
}

void NeuralNetwork::LearnMany(
        double eta,
        int iterations,
        const std::vector<std::vector<double>>& inputs,
        const std::vector<std::vector<double>>& correct_outputs) {

    for (int i = 0; i < iterations; ++i) {
        for (int j = 0; j < static_cast<int>(inputs.size()); ++j) {
            LearnOne(eta, inputs[j], correct_outputs[j]);
        }
    }

}


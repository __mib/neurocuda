#ifndef NEURAL_NETWORK_H
#define NEURAL_NETWORK_H
#include <cmath>
#include <string>
#include <vector>

class NeuralNetwork {
 public:
  // Creates a neural network with given layer sizes and random weights
  // from range [-1, 1].
  explicit NeuralNetwork(std::vector<int> layer_sizes);

  // Creates a neural network with given weight matrices. It takes ownership
  // of the wieght matrices.
  explicit NeuralNetwork(
      std::vector<int> layer_sizes,
      std::vector<double*> weights) :
    _layer_sizes(layer_sizes), _weights(weights) {}

  virtual ~NeuralNetwork();

  // Reads neural network from file. If success parameter is not null,
  // it will be set to false in case of error.
  // File format is the following:
  // <number of layers>
  // <number of neurons in each layer, space separated>
  // <weight matrices, separated by empty lines>
  static NeuralNetwork LoadFromFile(bool* success, std::string filename);

  // Saves neural network to file. Returns true on success.
  bool SaveToFile(std::string filename);

  int NumberOfLayers() const {
    return _weights.size() + 1;
  }

  // Returns output of the neural network for a given input vector.
  std::vector<double> GetOutput(const std::vector<double>& input) const;

  // Propagates input through the neural network and modifies
  // the weights to produce correct_output. Usually more than
  // one call to this method is required to for the neural network to
  // actually learn this input/output pair.
  void LearnOne(double eta,
      const std::vector<double>& input,
      const std::vector<double>& correct_output);

  void LearnMany(
      double eta,
      int iterations,
      const std::vector<std::vector<double>>& inputs,
      const std::vector<std::vector<double>>& correct_outputs);

  int getInputLayerSize() const;
  int getOutputLayerSize() const;
 
  std::vector<int> _layer_sizes;
  // Matrices for weights between consecutive layers (there are n-1 weights
  // matrices for n-layer network).
  // Each weight matrix is 2d array packed into 1d array row by row.
  // In i-th row there are weights for inputs to the i-th neuron in the output
  // layer.
  std::vector<double*> _weights;

  std::vector<double> PropagateOneLayer(
      const std::vector<double>& input,
      const double* weights,
      int output_size) const;

  double ActivationFunction(double x) const {
    return (1.0 / (1.0 + std::exp(-x)));
  }

  double ActivationFunctionDerivative(double x) const {
    double ex = std::exp(x);
    return ex / ((ex + 1) * (ex + 1));
  }
  std::vector<double> ApplyActivationFunction(const std::vector<double>& input) const;
  
  std::vector<double> _GetErrors(
      const std::vector<double>& correct,
      const std::vector<double>& input) const;

  std::vector<double> _PropagateErrors(
      const std::vector<double>& errors,
      const double* weights,
      const std::vector<double>& input) const;

  void _UpdateWeights(
      double eta,
      const std::vector<double>& errors,
      const std::vector<double>& outputs,
      double* weights,
      int input_size);

  double _MaxAbsoluteValue(const std::vector<double>& value);
};

#endif  // NEURAL_NETWORK_H

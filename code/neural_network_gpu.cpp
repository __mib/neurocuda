#include "neural_network_gpu.h"
#include <cuda.h>
#include <cstdlib>
#include <fstream>
#include <iostream>

using namespace std;

#define CHECK(u) { \
    CUresult cFRV = (u); \
    if (cFRV != CUDA_SUCCESS) { \
        printf("CUDA failed; LINE: %d, ERROR: %s\n", __LINE__, cuda_error_string(cFRV)); \
        exit(1); \
    } \
}

const char *cuda_error_string(CUresult result) { 
    switch(result) { 
    case CUDA_SUCCESS: return "No errors"; 
    case CUDA_ERROR_INVALID_VALUE: return "Invalid value"; 
    case CUDA_ERROR_OUT_OF_MEMORY: return "Out of memory"; 
    case CUDA_ERROR_NOT_INITIALIZED: return "Driver not initialized"; 
    case CUDA_ERROR_DEINITIALIZED: return "Driver deinitialized"; 

    case CUDA_ERROR_NO_DEVICE: return "No CUDA-capable device available"; 
    case CUDA_ERROR_INVALID_DEVICE: return "Invalid device"; 

    case CUDA_ERROR_INVALID_IMAGE: return "Invalid kernel image"; 
    case CUDA_ERROR_INVALID_CONTEXT: return "Invalid context"; 
    case CUDA_ERROR_CONTEXT_ALREADY_CURRENT: return "Context already current"; 
    case CUDA_ERROR_MAP_FAILED: return "Map failed"; 
    case CUDA_ERROR_UNMAP_FAILED: return "Unmap failed"; 
    case CUDA_ERROR_ARRAY_IS_MAPPED: return "Array is mapped"; 
    case CUDA_ERROR_ALREADY_MAPPED: return "Already mapped"; 
    case CUDA_ERROR_NO_BINARY_FOR_GPU: return "No binary for GPU"; 
    case CUDA_ERROR_ALREADY_ACQUIRED: return "Already acquired"; 
    case CUDA_ERROR_NOT_MAPPED: return "Not mapped"; 
    case CUDA_ERROR_NOT_MAPPED_AS_ARRAY: return "Mapped resource not available for access as an array"; 
    case CUDA_ERROR_NOT_MAPPED_AS_POINTER: return "Mapped resource not available for access as a pointer"; 
    case CUDA_ERROR_ECC_UNCORRECTABLE: return "Uncorrectable ECC error detected"; 
    case CUDA_ERROR_UNSUPPORTED_LIMIT: return "CUlimit not supported by device";    

    case CUDA_ERROR_INVALID_SOURCE: return "Invalid source"; 
    case CUDA_ERROR_FILE_NOT_FOUND: return "File not found"; 
    case CUDA_ERROR_SHARED_OBJECT_SYMBOL_NOT_FOUND: return "Link to a shared object failed to resolve"; 
    case CUDA_ERROR_SHARED_OBJECT_INIT_FAILED: return "Shared object initialization failed"; 

    case CUDA_ERROR_INVALID_HANDLE: return "Invalid handle"; 

    case CUDA_ERROR_NOT_FOUND: return "Not found"; 

    case CUDA_ERROR_NOT_READY: return "CUDA not ready"; 

    case CUDA_ERROR_LAUNCH_FAILED: return "Launch failed"; 
    case CUDA_ERROR_LAUNCH_OUT_OF_RESOURCES: return "Launch exceeded resources"; 
    case CUDA_ERROR_LAUNCH_TIMEOUT: return "Launch exceeded timeout"; 
    case CUDA_ERROR_LAUNCH_INCOMPATIBLE_TEXTURING: return "Launch with incompatible texturing"; 

    case CUDA_ERROR_UNKNOWN: return "Unknown error"; 

    default: return "Unknown CUDA error value"; 
    } 
}

void CudaVectorDeleter::operator()(CUdeviceptr* ptrptr) {
    if (ptrptr) {
        CUdeviceptr ptr = *ptrptr;
        if (ptr) CHECK(cuMemFree(ptr));
        delete ptrptr;
    }
}

CudaVector::CudaVector(double* w, int size_) : true_ptr(new CUdeviceptr, CudaVectorDeleter()), size(size_) {
    CUdeviceptr _ptr;
    CHECK(cuMemAlloc(&_ptr, sizeof(double) * size));
    CHECK(cuMemcpyHtoD(_ptr, w, sizeof(double) * size));
    *true_ptr = _ptr;
    //cout << "CudaVector(" << (int)(ptr) << ")" << endl;
}
CudaVector::CudaVector(const std::vector<double>& v) : true_ptr(new CUdeviceptr(), CudaVectorDeleter()),  size(v.size()) {
    CUdeviceptr _ptr;
    CHECK(cuMemAlloc(&_ptr, sizeof(double) * size));
    CHECK(cuMemcpyHtoD(_ptr, v.data(), sizeof(double) * size));
    *true_ptr = _ptr;
    //cout << "CudaVector(" << (int)(ptr) << ")" << endl;
}
CudaVector::CudaVector(int size_) : true_ptr(new CUdeviceptr, CudaVectorDeleter()), size(size_) {
    CUdeviceptr _ptr;
    CHECK(cuMemAlloc(&_ptr, sizeof(double) * size));
    *true_ptr = _ptr;
    //cout << "CudaVector(" << (int)(ptr) << ")" << endl;
}

CUdeviceptr* const CudaVector::ptr() const {
    return true_ptr.get();
}

/*CudaVector::~CudaVector() {
    //cout << "~CudaVector(" << (int)(ptr) << ")" << endl;
    if (ptr)
        CHECK(cuMemFree(ptr));
}*/
std::vector<double> CudaVector::vector() const {
    std::vector<double> v(size);
    CHECK(cuMemcpyDtoH(v.data(), *true_ptr, sizeof(double) * size));
    return v;
}
/*CudaVector::CudaVector(const CudaVector b) : ptr()(0), size(b.size) {
    CHECK(cuMemAlloc(&ptr, sizeof(double) * size));
    CHECK(cuMemcpyDtoD(ptr, b.ptr, sizeof(double) * size));
}
CudaVector& CudaVector::operator=(const CudaVector& b) {
    if (ptr) CHECK(cuMemFree(ptr));
    size = b.size;
    CHECK(cuMemAlloc(&ptr, sizeof(double) * size));
    CHECK(cuMemcpyDtoD(ptr, b.ptr, sizeof(double) * size));
    return *this;
}*/

template <typename T>
std::ostream& operator<<(std::ostream& out, const std::vector<T>& in) {
    out << "[";
    for (const T& t : in) {
        out << t << ", ";
    }
    return out << "]";
}

CUdevice NeuralNetwork::cuDevice;
CUcontext NeuralNetwork::cuContext;
CUmodule NeuralNetwork::cuModule;
bool NeuralNetwork::initialized;

void NeuralNetwork::Init() {
    if (initialized) return;
    CHECK(cuInit(0));
    CHECK(cuDeviceGet(&cuDevice, 0));
    CHECK(cuCtxCreate(&cuContext, 0, cuDevice));
    CHECK(cuModuleLoad(&cuModule, "gen/neural_network_gpu.ptx"));
    initialized = true;
}

NeuralNetwork::NeuralNetwork(
  std::vector<int> layer_sizes,
  std::vector<double*> weights) :
_layer_sizes(layer_sizes), _weights() {
    Init();
    for (int i = 0; i < static_cast<int>(weights.size()); ++i) {
        _weights.push_back(CudaVector(weights[i], _layer_sizes[i] * _layer_sizes[i+1]));
    }

}

void NeuralNetwork::Destroy() {
    /*CHECK(cuModuleUnload(cuModule));
    CHECK(cuCtxDestroy(cuContext));*/
}

NeuralNetwork::NeuralNetwork(std::vector<int> layer_sizes) :
        _layer_sizes(layer_sizes), _weights() {
    Init();
    for (int i = 1; i < static_cast<int>(layer_sizes.size()); ++i) {
        int matrix_size = layer_sizes[i-1] * layer_sizes[i];
        double* weight_matrix = new double[matrix_size];
        for (int j = 0; j < matrix_size; ++j) {
            weight_matrix[j] = -1.0 + static_cast<double>(rand()) / (RAND_MAX / 2);
        }
        _weights.push_back(CudaVector(weight_matrix, matrix_size));
    }
}

NeuralNetwork::~NeuralNetwork() {
    Destroy();
}

int NeuralNetwork::getInputLayerSize() const {
    return _layer_sizes.size() > 0 ? _layer_sizes[0] : 0;
}

int NeuralNetwork::getOutputLayerSize() const {
    return _layer_sizes.size() > 0 ? _layer_sizes[_layer_sizes.size()-1] : 0;
}

int _ReadPositiveInt(bool* error, std::ifstream* file) {
    int result;
    (*file) >> result;
    if (result <= 0 or file->fail()) {
        *error = true;
    }
    return result;
}

double _ReadWeight(bool* error, std::ifstream* file) {
    double weight;
    (*file) >> weight;
    if (file->fail()) {
      *error = true;
    }
    return weight;
}

NeuralNetwork NeuralNetwork::LoadFromFile(bool* success, std::string filename) {
    std::vector<int> layer_sizes;
    std::vector<double*> weights;
    int layers;
    bool error = false;
    std::ifstream file;
    file.open(filename);
    if (not file.is_open()) {
        error = true;
    } else {
        layers = _ReadPositiveInt(&error, &file);
        if (not error) {
            for (int i = 0; i < layers; ++i) {
                int layer_size = _ReadPositiveInt(&error, &file);
                layer_sizes.push_back(layer_size);
                if (error) {
                    break;
                }
            }
        }
        if (not error) {
            for (int i = 1; i < static_cast<int>(layer_sizes.size()); ++i) {
                int weights_matrix_size = layer_sizes[i - 1] * layer_sizes[i];
                double* weights_matrix = new double[weights_matrix_size];
                weights.push_back(weights_matrix);
                for (int j = 0; j < weights_matrix_size; ++j) {
                    weights_matrix[j] = _ReadWeight(&error, &file);
                    if (error) {
                        break;
                    }
                }
                if (error) break;
            }
        }
    }
    if (error) {
        if (success != NULL) {
            *success = false;
        }
        for (double* weight : weights) {
            delete[] weight;
        }
        return NeuralNetwork({1});
    }
    NeuralNetwork result(layer_sizes, weights);
    for (double* weight : weights) {
        delete[] weight;
    }
    return result;
}

bool NeuralNetwork::SaveToFile(std::string filename) {
    std::ofstream file;
    file.open(filename);
    file << NumberOfLayers() << std::endl;
    bool first = true;
    for (int layer_size : _layer_sizes) {
        if (not first) file << " ";
        first = false;
        file << layer_size;
    }
    file << std::endl;
    for (int i = 1; i < static_cast<int>(_layer_sizes.size()); ++i) {
        file << std::endl;
        int idx = 0;
        vector<double> weights = _weights[i-1].vector();
        for (int j = 0; j < _layer_sizes[i]; ++j) {
            first = true;
            for (int k = 0; k < _layer_sizes[i-1]; ++k) {
                if (not first) file << " ";
                first = false;
                file << weights[idx++];
            }
            file << std::endl;
        }
    }
    return file.good();
}

CudaVector NeuralNetwork::PropagateOneLayer(
        const CudaVector& input,
        const CudaVector& weights,
        int output_size) const {

    CudaVector output(output_size);
    void* args[] = {
        (void*)output.ptr(),
        (void*)&output.size,
        (void*)input.ptr(),
        (void*)&input.size,
        (void*)weights.ptr()
    };

    CUfunction propagateOneLayer;
    CHECK(cuModuleGetFunction(&propagateOneLayer, cuModule, "PropagateOneLayer"));

    int block_size = 128;

    int block_count = (output_size + block_size - 1) / block_size;

    CHECK(cuLaunchKernel(propagateOneLayer,
                block_count, 1, 1,
                block_size, 1, 1,
                0, 0, args, 0));
    CHECK(cuCtxSynchronize());
    
    return output;
}

CudaVector NeuralNetwork::ApplyActivationFunction(const CudaVector& input) const {
    CudaVector output(input.size);
    void* args[] = {
        (void*)input.ptr(),
        (void*)output.ptr(),
        (void*)&input.size,
    };

    CUfunction applyActivationFunction;
    CHECK(cuModuleGetFunction(&applyActivationFunction, cuModule, "ApplyActivationFunction"));

    int block_size = 128;

    int block_count = (input.size + block_size - 1) / block_size;

    CHECK(cuLaunchKernel(applyActivationFunction,
                block_count, 1, 1,
                block_size, 1, 1,
                0, 0, args, 0));
    CHECK(cuCtxSynchronize());
    return output;
}

std::vector<double> NeuralNetwork::GetOutput(
        const std::vector<double>& input) const {
    std::vector<double> output = input;
    for (int i = 1; i < NumberOfLayers(); ++i) {
        output = ApplyActivationFunction(
                PropagateOneLayer(CudaVector(output),
                    _weights[i-1],
                    _layer_sizes[i])).vector();
    }
    return output;
}

CudaVector NeuralNetwork::_GetErrors(
        const CudaVector& correct,
        const CudaVector& input) const {
    CudaVector errors(correct.size);
    void* args[] = {
        (void*)input.ptr(),
        (void*)correct.ptr(),
        (void*)errors.ptr(),
        (void*)&input.size,
    };
    CUfunction getErrors;
    CHECK(cuModuleGetFunction(&getErrors, cuModule, "GetErrors"));
    int block_size = 128;
    int block_count = (input.size + block_size - 1) / block_size;
    CHECK(cuLaunchKernel(getErrors,
                block_count, 1, 1,
                block_size, 1, 1,
                0, 0, args, 0));
    CHECK(cuCtxSynchronize());
    return errors;
}

CudaVector NeuralNetwork::_PropagateErrors(
        const CudaVector& errors,
        const CudaVector& weights,
        const CudaVector& input) const {
    CudaVector input_errors(input.size);
    void* args[] = {
        (void*)input.ptr(),
        (void*)&input.size,
        (void*)input_errors.ptr(),
        (void*)errors.ptr(),
        (void*)&errors.size,
        (void*)weights.ptr()
    };
    CUfunction propagateErrors;
    CHECK(cuModuleGetFunction(&propagateErrors, cuModule, "PropagateErrors"));
    int block_size = 128;
    int block_count = (input.size + block_size - 1) / block_size;
    CHECK(cuLaunchKernel(propagateErrors,
                block_count, 1, 1,
                block_size, 1, 1,
                0, 0, args, 0));
    CHECK(cuCtxSynchronize());
    return input_errors;
    /*
    int idx = 0;
    for (int i = 0; i < static_cast<int>(errors.size()); ++i) {
        for (int j = 0; j < static_cast<int>(input.size()); ++j) {
            input_errors[j] += weights[idx] * errors[i];
            idx++;
        }
    }
    idx = 0;
    for (double& input_error : input_errors) {
        input_error *= ActivationFunctionDerivative(input[idx]);
        idx++;
    }
    */
}

void NeuralNetwork::_UpdateWeights(
        double eta,
        const CudaVector& errors,
        const CudaVector& outputs,
        const CudaVector& weights,
        int input_size) {
    void* args[] = {
        (void*)&eta,
        (void*)errors.ptr(),
        (void*)&errors.size,
        (void*)outputs.ptr(),
        (void*)&input_size,
        (void*)weights.ptr(),
    };
    CUfunction updateWeights;
    CHECK(cuModuleGetFunction(&updateWeights, cuModule, "UpdateWeights"));
    int block_size = 128;
    int block_count = (errors.size + block_size - 1) / block_size;
    CHECK(cuLaunchKernel(updateWeights,
                block_count, 1, 1,
                block_size, 1, 1,
                0, 0, args, 0));
    CHECK(cuCtxSynchronize());
}
double NeuralNetwork::_MaxAbsoluteValue(
        const std::vector<double>& value) {
    double res = 0;
    for (double v : value) {
        if (std::fabs(v) > res) res = std::fabs(v);
    }
    return res;
}

void NeuralNetwork::LearnOne(double eta,
        const std::vector<double>& input,
        const std::vector<double>& correct_output) {
    std::vector<CudaVector> outputs;
    outputs.push_back(CudaVector(input));
    for (int i = 1; i < NumberOfLayers(); ++i) {
        if (i == 1) {
            //cout << "INPUT:" << endl;
            //cout << outputs.back() << endl;
            CudaVector new_output(PropagateOneLayer(
                        outputs.back(),
                        _weights[i-1],
                        _layer_sizes[i]));
            outputs.push_back(new_output);
            ////cout << "OUTPUT:" << endl;
            //cout << outputs.back() << endl;
            //cout << "actual:" << endl;
            //cout << ApplyActivationFunction(outputs.back()) << endl;
        } else {
            CudaVector new_output = PropagateOneLayer(
                        ApplyActivationFunction(outputs.back()),
                        _weights[i-1],
                        _layer_sizes[i]);
            outputs.push_back(new_output);
        }
    }
    std::vector<CudaVector> errors(NumberOfLayers());
    {
        CudaVector error = _GetErrors(CudaVector(correct_output), outputs.back());
        errors.back() = error;
    }
    //cout << "first error: " << errors.back() << endl;
    for (int i = NumberOfLayers() - 1; i > 0; --i) {
        {
            CudaVector error(_PropagateErrors(errors[i], _weights[i-1], outputs[i-1]));
            errors[i-1] = error;
        }
        //cout << "next error: " << errors[i-1] << endl;
        if (i > 1) {
            _UpdateWeights(eta, errors[i], ApplyActivationFunction(outputs[i-1]),
                    _weights[i-1],
                    _layer_sizes[i-1]);
        } else {
            _UpdateWeights(eta, errors[i], outputs[i-1],
                    _weights[i-1],
                    _layer_sizes[i-1]);
        }
    }

    /*
       cout << "weights:" << endl;
       for(int i = 0; i < _weights.size(); i++) {
       for(int j = 0; j < _layer_sizes[i]; j++) {
       cout << _weights[i][j] << " ";
       }
       cout << endl;
       }
       */
}

void NeuralNetwork::LearnMany(
        double eta,
        int iterations,
        const std::vector<std::vector<double>>& inputs,
        const std::vector<std::vector<double>>& correct_outputs) {

    for (int i = 0; i < iterations; ++i) {
        for (int j = 0; j < static_cast<int>(inputs.size()); ++j) {
            LearnOne(eta, inputs[j], correct_outputs[j]);
        }
    }

}


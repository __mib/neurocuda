#!/usr/bin/env python3
# -*- coding: utf-8 -*- 
import httplib2

NAMES = {
        # Current:
        "Adrian": "Marek Adrian",
        "Borak": "Szymon Borak",
        "Bosek": "Bartłomiej Bosek",
        "Chmaj": "Maria Chmaj",
        "Chociej": "Maciej Chociej",
        "Cieślik": "Iwona Cieślik",
        "Duraj": "Lech Duraj",
        "Farnik": "Michał Farnik",
        "Gagol": "Adam Gągol",
        "Gillert": "Monika Gillert",
        "Grygiel": "Katarzyna Grygiel",
        "Grytczuk": "Jarosław Grytczuk",
        "Grzesik": "Andrzej Grzesik",
        "Gutowski": "Grzegorz Gutowski",
        "Herman": "Grzegorz Herman",
        "Horwath": "Leszek Horwath",
        "Idziak": "Pawel M. Idziak",
        "Kosinski": "Karol Kosiński",
        "Kostanek": "Mateusz Kostanek",
        "Kozik": "Marcin Kozik",
        "KozikJ": "Jakub Kozik",
        "Krawczyk": "Tomasz Krawczyk",
        "lachowski": "Łukasz Lachowski",
        "Lason": "Michał Lasoń",
        "Lubawski": "Wojciech Lubawski",
        "Lupinska": "Agnieszka Łupińska",
        "Matecki": "Grzegorz Matecki",
        "Micek": "Piotr Micek",
        "Obryk": "Robert Obryk",
        "pasek": "Krzysztof Pasek",
        "Pawlik": "Arkadiusz Pawlik",
        "Pezarski": "Andrzej Pezarski",
        "Staromiejski": "Michał Staromiejski",
        "Szafruga": "Piotr Szafruga",
        "Szczypka": "Edward Szczypka",
        "Ślusarek": "Maciej Ślusarek",
        "Walczak": "Bartosz Walczak",
        "Zaionc": "Marek Zaionc",
        "Zmarz": "Michał Zmarz",
        # Former:
        "Bilski": "Marcin Bilski",
        "Broniek": "Przemysław Broniek",
        "jarek": "Jarosław Duda",
        "Gorazd": "Tomasz Gorazd",
        "Hajto": "Zbigniew Hajto",
        "Handzlik": "Michał Handzlik",
        "Jezabek": "Jan Jeżabek",
        "Kloch": "Kamil Kloch",
        "Kolany": "Adam Kolany",
        "Krzaczkowski": "Jacek Krzaczkowski",
        "Lescanne": "Pierre Lescanne",
        "Pudo": "Mikołaj Pudo",
        "Sedziwy": "Stanisław Sędziwy",
        "Siedlarz": "Joanna Siedlarz",
        "Waszkiewicz": "Paweł Waszkiewicz",
        "Zelazny": "Wiktor Żelazny",
}

IMAGE_URL_PATTERN = "http://tcs.uj.edu.pl/pictures.php?surname={login}"

HTTP_HANDLER = httplib2.Http('/tmp/.cache')

def convert_to_grayscale(image_data):
    try:
        from PIL import Image
        import io
        image = Image.open(io.BytesIO(image_data))
        output_stream = io.BytesIO()
        grayscale_image = image.convert("L")
        grayscale_image.save(output_stream, format="jpeg")
        output_stream.flush()
        return output_stream.getvalue()
    except Exception as e:
        print("Error when converting to grayscale: {0}".format(e))
        return image_data

def get_image(login):
    response, content = HTTP_HANDLER.request(IMAGE_URL_PATTERN.format(login=login))
    if response.status == 200 and len(content):
        return content
    else:
        print("Error getting {2}: (response.status: {0}, len(content): {1})"
                .format(response.status, len(content), login))

def get_images(grayscale=False):
    for login in NAMES.keys():
        image = get_image(login)
        if image:
            if grayscale: image = convert_to_grayscale(image)
            with open("{0}.jpg".format(login), "wb") as f:
                f.write(image)

if __name__ == "__main__":
    get_images(True)

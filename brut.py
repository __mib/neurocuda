import math
import copy
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

w = [
    [
        [0.2, 0.5],
        [0.7, 0.3],
        [0.3, 0.6],
    ],
    [
        [0.1, 0.2, 0.5],
    ]
]

a = [0.4, 0.6]

def sigmoid(x):
    return 1/(1 + math.e**(-x))

def sigmoidprime(x):
    return math.e**x / ((math.e**x + 1)**2)

def propagate(a, w):
    o = [0] * len(w)
    for i in range(len(a)):
        for j in range(len(w)):
            o[j] += w[j][i] * a[i]
    return o

def activate(a):
    return map(sigmoid, a)

def getoutput(a, w):
    data = a
    for i in range(len(w)):
        data = activate(propagate(data, w[i]))
    return data

propagate(a, w[0])

data = a
output = [0.3]

def first_error(comb, correct):
    output = activate(comb)
    der = [sigmoidprime(arg) for arg in comb]
    return [-(correct[i] - output[i]) * der[i] for i in range(0, len(comb))]

def propagate_err(err, w, comb):
    der = [sigmoidprime(arg) for arg in comb]
    new_err = [0] * len(comb)
    for i in range(0, len(comb)):
        for j in range(0, len(w)):
            new_err[i] += w[j][i] * err[j]
    for i in range(0, len(comb)):
        new_err[i] *= der[i]
    return new_err

def learn(w, data, output, alpha):
    combs = [data]
    for i in range(len(w)):
        #print "INPUT:"
        #print combs[-1]
        combs.append(propagate(data, w[i]))
        data = activate(propagate(data, w[i]))
        #print "OUTPUT:"
        #print combs[-1]

    #print "correct: ", output
    #print "actual: ", activate(combs[-1])
    #print "comb: ", combs[-1]
    errs = []
    err = first_error(combs[-1], output)
    #print "first err: ", err

    dw = copy.deepcopy(w)

    for i in range(len(w)-1, -1, -1):
        if i != 0:
            a = activate(combs[i])
        else:
            a = combs[i]
        #print "eta: ", alpha, " errors: ", err, "input: ", a
        for j in range(len(a)):
            for k in range(len(combs[i+1])):
                dw[i][k][j] = a[j] * err[k]
        err = propagate_err(err, w[i], combs[i])
        #print "next err: ", err

    for i in range(0, len(w)):
        for j in range(0, len(w[i])):
            for k in range(0, len(w[i][j])):
                w[i][j][k] -= alpha * dw[i][j][k];

iters = 10000
N = 10

X = [1.0/(N+1) * i for i in range(0, N)]
Y = [1 - 1.0/(N+1) * i for i in range(0, N)]

data = [[1, X[i]] for i in range(0, N)]
output = [[Y[i]] for i in range(0, N)]

plt.scatter(X, Y, color='red')

w = [[[1, -1]]]
for i in range(0, iters): 
    for j in range(0, N):
        learn(w, data[j], output[j], 0.1)
        print "weights:", w

outs = [getoutput(data[i], w) for i in range(0, N)]

plt.scatter(X, outs, color='blue')

plt.savefig("gen/sin.png")
